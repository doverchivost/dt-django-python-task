from django.contrib import admin

from app.internal.models.person import Person


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    pass
