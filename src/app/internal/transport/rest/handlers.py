from app.internal.services import user_service
from django.http import HttpResponse

def me(request):
    tg_id = request.GET.get('telegram_id', None)
    if user_service.tg_id_exist(tg_id):
        user = user_service.get_user_info(tg_id)
        resp = f"Данные в базе данных для данного пользователя: \n\n" \
              f"Имя: {user[0]} \n" \
               f"Telegram ID: {user[1]} \n" \
               f"Номер телефона: {user[2]} \n"
        return HttpResponse(resp, content_type="text/plain; charset=utf-8")
    return HttpResponse("Пользователя с таким Telegram ID в базе данных нет")


#def get