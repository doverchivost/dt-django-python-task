from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import KeyboardButton, ReplyKeyboardMarkup

from config.settings import TELEGRAM_TOKEN
from app.internal.services import user_service


def start():
    updater = Updater(TELEGRAM_TOKEN)

    updater.dispatcher.add_handler(CommandHandler("start", greeting))
    updater.dispatcher.add_handler(CommandHandler("set_phone", ask_phone_number))
    updater.dispatcher.add_handler(CommandHandler("me", send_user_info))

    updater.dispatcher.add_handler(MessageHandler(Filters.contact, add_phone))
    updater.dispatcher.add_handler(MessageHandler(Filters.text, simple_text_message))

    updater.start_polling()
    updater.idle()


def greeting(update, context):
    username = str(update.effective_user.username)
    id = int(update.effective_user.id)
    #СОХРАНИТЬ В БД username и id
    if not user_service.user_exist(id, username):
        user_service.add_user(username, id)
        update.message.reply_text("Привет, " + username + "! \nЯ сохранил тебя в БД")
    else:
        update.message.reply_text("Привет, " + username + "! \nТы уже есть в БД")


def ask_phone_number(update, context):
    username = str(update.effective_user.username)
    id = int(update.effective_user.id)
    contact_keyboard = KeyboardButton('Отправить свой номер телефона', request_contact=True)  # creating contact button object
    custom_keyboard = [[contact_keyboard]]  # creating keyboard object
    reply_markup = ReplyKeyboardMarkup(custom_keyboard)
    update.message.reply_text(
        "Нажми на кнопку, чтобы отправить мне свой номер телефона",
        reply_markup=reply_markup)


def add_phone(update, context):
    username = str(update.effective_user.username)
    id = int(update.effective_user.id)
    contact = update.effective_message.contact
    phone = str(contact.phone_number)
    # СОХРАНИТЬ В БД к username и id еще и телефон
    user_service.add_phone(id, phone)
    update.message.reply_text("Твой телефон " + phone + "\nЯ сохранил его в БД")


def send_user_info(update, context):
    username = str(update.effective_user.username)
    id = int(update.effective_user.id)
    #ПРОВЕРКА телефон в бд есть?
    has_phone_number = user_service.has_phone(id)
    if has_phone_number:
        name_id_phone = user_service.get_user_info(id)
        username = name_id_phone[0]
        if username == "None":
            username = "(у Telegram-аккаунта нет имени)"
        tg_id = name_id_phone[1]
        telephone_number = name_id_phone[2]
        reply = f"Твои данные в БД: \n\n " \
                f"Имя: {username}\n" \
                f"telegram ID: {tg_id}\n" \
                f"Номер телефона: {telephone_number}"
        update.message.reply_text(reply)
    else:
        update.message.reply_text("Сперва добавь свой номер телефона")


def simple_text_message(update, context):
    update.message.reply_text("Я пока работаю только с командами")
