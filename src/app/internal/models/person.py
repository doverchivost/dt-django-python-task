from django.db import models


class Person(models.Model):
    name = models.CharField(max_length=255)
    tg_id = models.IntegerField(unique=True)
    telephone = models.CharField(max_length=20, blank=True)
