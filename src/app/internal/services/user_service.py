from app.internal.models.person import Person


def add_user(tg_username, tg_id):
    user = Person(name=tg_username, tg_id=tg_id)
    user.save()
    return user


def tg_id_exist(tg_id):
    if Person.objects.filter(tg_id=tg_id).exists():
        return True
    return False


def user_exist(tg_id, username):
    if Person.objects.filter(tg_id=tg_id).exists():
        user = Person.objects.get(tg_id=tg_id)
        if user.name != username:
            user.name = username
            user.save()
        return True
    return False


def add_phone(tg_id, phone):
    user = Person.objects.get(tg_id=tg_id)
    user.telephone = phone
    user.save()
    return user


def has_phone(tg_id):
    user = Person.objects.get(tg_id=tg_id)
    phone = user.telephone
    if len(phone) < 1:
        return False
    return True


def get_user_info(tg_id):
    user = Person.objects.get(tg_id=tg_id)
    name_id_phone = [user.name, str(user.tg_id), user.telephone]
    return name_id_phone

